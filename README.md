# Phonebook app
`phonebook` is an simple application for saving contact details and search for saved contact details.

## Getting started
Import the project in netbeans IDE and import following dependent libraries for successfully build the project.

* sqlite-jdbc-3.7.2.jar
* WebLookAndFeel.jar

After importing the above mentioned libraries one can update the database location which uses the Phonebook app inside the `src/DB/DBConnManager.java` file. Once the above configuration is done user can build the project. Once the project is successfully built navigate to the dist folder and run the `Phonebooknew.jar` file.

sqlite database has been attached with this inside the root folder so user can put it in the location where, was configured inside the `DBConnManager.java` file.