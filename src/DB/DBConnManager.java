package DB;

 

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;


public class DBConnManager {

    String sourceurl;

    public DBConnManager() {
        try 
        {
            Class.forName("org.sqlite.JDBC");
             
             
        } 
        catch (ClassNotFoundException ex)
        {
             
            JOptionPane.showMessageDialog(null, "construter:- "+ex);
        }
        catch(Exception ex)
        {
            System.out.println(ex);
        }

    }

    public Connection getConnect() {
        Connection dbConn = null;
        try {
            //C:\Program Files\PhoneBook
            //C:\\Users\\Niroshan\\Documents\\NetBeansProjects\\PhoneBookNew\\mydb.sqlite
           // dbConn = DriverManager.getConnection("jdbc:sqlite:C:\\Program Files\\PhoneBook\\PhoneBook.sqlite");
            dbConn = DriverManager.getConnection("jdbc:sqlite:mydb.sqlite");
            
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null , ex+"Get connect");
        }
        return dbConn;
    }

    public void closeConnection(Connection dbConn)throws SQLException{
        try {
            dbConn.close();
        } catch (SQLException ex) {
             System.out.println(ex);
        }
    }
}
