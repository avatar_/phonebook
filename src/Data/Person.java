/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

/**
 *
 * @author Niroshan
 */
public class Person 
{
    String fname;
    String lname;
    String mobile;
    String home;
    String work;
    String fax;
    String email;


     
    byte[] image_det ;

    public Person(String pFname,String pLname,String pMobile,String pHome,String pWork,String pFax,byte[] image,String pEmail)
    {
        this.fname = pFname;
        this.lname = pLname;
        this.mobile = pMobile;
        this.home = pHome;
        this.work = pWork;
        this.fax = pFax;
        this.image_det = image;
        this.email = pEmail;
        
    }
     public Person(String pFname,String pLname,String pMobile,String pHome,String pWork,String pFax,String pEmail)
    {
        this.fname = pFname;
        this.lname = pLname;
        this.mobile = pMobile;
        this.home = pHome;
        this.work = pWork;
        this.fax = pFax;
        this.email = pEmail;
        
        
    }
       
     public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Person() {
         
    }
    
    
    public String getFax()
    {
        return fax;
    }

    public void setFax(String fax) 
    {
        this.fax = fax;
    }

    public String getFname()
    {
        return fname;
    }

    public void setFname(String fname)
    {
        this.fname = fname;
    }

    public String getHome()
    {
        return home;
    }

    public void setHome(String home)
    {
        this.home = home;
    }

    public byte[] getImage_det() {
        return image_det;
    }

    public void setImage_det(byte[] image_det) {
        this.image_det = image_det;
    }

   

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) 
    {
        this.lname = lname;
    }

    public String getMobile()
    {
        return mobile;
    }

    public void setMobile(String mobile)
    {
        this.mobile = mobile;
    }

    public String getWork() 
    {
        return work;
    }

    public void setWork(String work)
    {
        this.work = work;
    }
    
    
}
