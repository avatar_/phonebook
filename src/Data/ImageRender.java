/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import java.awt.Component;
import java.awt.image.BufferedImage;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author Niroshan
 */
public class ImageRender  extends DefaultTableCellRenderer
{ 
   @Override
 	    public Component getTableCellRendererComponent(JTable table,Object value, boolean isSelected,boolean hasFocus, int row, int column)
 	    {
 	        JLabel label = new JLabel();
 	 
 	        if (value!=null) {
 	        label.setHorizontalAlignment(JLabel.CENTER);
 	        //value is parameter which filled by byteOfImage
 	        label.setIcon(new ImageIcon((byte[])value));
 	        }
 	 
 	        return label;
 	    }

   
     
     
    
 

}
