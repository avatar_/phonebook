/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import DB.DBConnManager;
import java.sql.*;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Niroshan
 */
public class PersonDAO 
{
    DBConnManager dbconn ;
    
    public PersonDAO()
    {
        dbconn = new DBConnManager();
    }
    
    
    public int  InsertPerson1(Person p)
    {
        int status=2;
        Connection con = null;
        try 
        {
            dbconn = new DBConnManager();
            con = dbconn.getConnect();
           
            Statement stmt = con.createStatement();
            Statement stmt2 = con.createStatement();
 
            String fname = p.fname;
            String lname = p.lname;
            byte[] image = p.image_det;
            String mob = p.mobile;
            String wor = p.work;
            String hom = p.home;
            String fax = p.fax;
            String email = p.email;
            int pID ;
            
            ResultSet rst = stmt.executeQuery("SELECT MAX(pID) FROM person");
            ResultSet rstN = stmt2.executeQuery("SELECT F_name,L_name FROM person WHERE F_name = '"+fname+"' AND L_name ='"+lname+"'");
            
            if(rstN.next())
            {
   
                status = 1;
            }
            else
            {
                pID =    Integer.parseInt(rst.getString(1))+1;
                PreparedStatement pstmt = con.prepareStatement("INSERT INTO person(pID,F_name,L_name,image) VALUES (?,?,?,?)");
                pstmt.setInt(1, pID);
                pstmt.setString(2, fname);
                pstmt.setString(3, lname);
                pstmt.setBytes(4, image);
              
                pstmt.executeUpdate();
         
                Statement stmt1 = con.createStatement();
                
 
             
                if(isEmpty(mob)== false)
                {
                stmt1.executeUpdate("INSERT INTO contact(pID,number,type)VALUES("+pID+",'"+mob+"','Mobile' )   ");   
                }

                if(isEmpty(wor)== false)
                {
                    stmt1.executeUpdate("INSERT INTO contact(pID,number,type)VALUES("+pID+",'"+wor+"','Work' )   ");   
                }
                if(isEmpty(hom)== false)
                {
                    stmt1.executeUpdate("INSERT INTO contact(pID,number,type)VALUES("+pID+",'"+hom+"','Home' )   ");   
                }
                if(isEmpty(fax)== false)
                {
                    stmt1.executeUpdate("INSERT INTO contact(pID,number,type)VALUES("+pID+",'"+fax+"','Fax' )   ");   
                }
                if(isEmpty(email)== false)
                {
                    stmt1.executeUpdate("INSERT INTO contact(pID,number,type) VALUES ('"+pID+"','"+email+"','Email')");
                }


                status = 0;
            }
            
            
          con.close();  
        } 
        catch (SQLException ex)
        {
            System.out.println(ex);
            status = 2;
        }
        
        return status;
    }
    
    
    //////////////////////// inser if haven't image
    public int  InsertPerson2(Person p)
    {
        int status = 2  ;
        Connection con = null;
        try 
        {
            dbconn = new DBConnManager();
            con = dbconn.getConnect();
           
            Statement stmt = con.createStatement();
            Statement stmt2 =con.createStatement();
 
            String fname = p.fname;
            String lname = p.lname;      
            String mob = p.mobile;
            String wor = p.work;
            String hom = p.home;
            String fax = p.fax;
            String email = p.email;
            int pID ;
            

                ResultSet rst = stmt.executeQuery("SELECT MAX(pID) FROM person");
                ResultSet rstN = stmt2.executeQuery("SELECT F_name,L_name FROM person WHERE F_name = '"+fname+"' AND L_name ='"+lname+"'");


                if(rstN.next())
                {
 
                    status = 1;
                    
                }
                else
                {
                    pID =    Integer.parseInt(rst.getString(1))+1;
                    PreparedStatement pstm2 = con.prepareStatement("INSERT INTO person(pID,F_name,L_name) VALUES (?,?,?)");
                    pstm2.setInt(1, pID);
                    pstm2.setString(2, fname);
                    pstm2.setString(3, lname);


                    pstm2.executeUpdate();


                    Statement stmt1 = con.createStatement();


                    if(isEmpty(mob)== false)
                    {
                    stmt1.executeUpdate("INSERT INTO contact(pID,number,type)VALUES("+pID+",'"+mob+"','Mobile' )   ");   
                    }

                    if(isEmpty(wor)== false)
                    {
                        stmt1.executeUpdate("INSERT INTO contact(pID,number,type)VALUES("+pID+",'"+wor+"','Work' )   ");   
                    }
                    if(isEmpty(hom)== false)
                    {
                        stmt1.executeUpdate("INSERT INTO contact(pID,number,type)VALUES("+pID+",'"+hom+"','Home' )   ");   
                    }
                    if(isEmpty(fax)== false)
                    {
                        stmt1.executeUpdate("INSERT INTO contact(pID,number,type)VALUES("+pID+",'"+fax+"','Fax' )   ");   
                    }
                    if(isEmpty(email)== false)
                    {
                        stmt1.executeUpdate("INSERT INTO contact(pID,number,type) VALUES ('"+pID+"','"+email+"','Email')");
                    }
                    status = 0;
                }

                
            
            con.close();
            
        } 
        catch (SQLException ex)
        {
            System.out.println(ex);
            status = 2;
        }
         
        return status;
    }
    
    
    public Object searchPerson(String name)
    {
        String[] fullname= name.split(" ");
        String fname = fullname[0];
        String lname = fullname[1];
        Object ob[] = new Object[9];
 
        
        try
        {
            Connection conn = dbconn.getConnect();
            
            Statement stmt = conn.createStatement();
            
             
           ResultSet rst = stmt.executeQuery("SELECT pID FROM person WHERE F_name = '"+fname+"' AND L_name = '"+lname+"'");
            int pid = Integer.parseInt(rst.getString(1).toString());
            
            ResultSet rst1 = stmt.executeQuery("SELECT P.pID, P.F_name, P.L_name, P.image,"
                    + "Mob.number AS Mobile,"
                    + "Wor.number AS Work,"
                    + "Hom.number AS Home,"
                    + "Fax.number AS Fax,"
                    + "Ema.number AS Email "
                    + "FROM  person AS  P "
                    + "LEFT JOIN contact AS Mob ON Mob.pID = P.pID AND  Mob.type = 'Mobile'"
                    + "LEFT JOIN contact AS Wor ON Wor.pID = P.pID AND  Wor.type = 'Work' "
                    + "LEFT JOIN contact AS Hom ON Hom.pID = P.pID AND  Hom.type = 'Home'"
                    + "LEFT JOIN contact AS Fax ON Fax.pID = P.pID AND  Fax.type = 'Fax'"
                    + "LEFT JOIN contact AS Ema ON Ema.pID = P.pID AND  Ema.type = 'Email'"
                    + " WHERE P.pID  = "+pid+" ");
            
     
            if(rst.next())
            {
 
                ob[0] = rst1.getString(1);
                ob[1] = rst1.getString(2);
                ob[2] = rst1.getString(3);
                ob[3] = rst1.getByte(4);
                ob[4] = rst1.getString(5);
                ob[5] = rst1.getString(6);
                ob[6] = rst1.getString(7);
                ob[7] = rst1.getString(8);
                ob[8] = rst1.getString(9);
 
            }
            conn.close();
 
        }
        catch(Exception ex)
        {
            System.out.println(ex+"sfa");
        }
 
        
        return ob;  
    }
    
    public void setTabledetailsByFname(String name,JTable jTable1) throws SQLException
{
 
        try 
            
        {
            DefaultTableModel dftm = (DefaultTableModel) jTable1.getModel();
 
            while(dftm.getRowCount()>0)
            {
                dftm.removeRow(0);
            }
             
            DBConnManager dbconn = new DBConnManager();
            Connection conn = dbconn.getConnect();
            
            Statement stmt = conn.createStatement();
            
            ResultSet rst = stmt.executeQuery("SELECT image,F_name,L_name FROM person WHERE F_name LIKE '"+name+"%'  ");
            
            while(rst.next())
            {
                
                byte[]imagedata = rst.getBytes(1); // get image data to byte array

                String perName = rst.getString(2).toString()+" "+rst.getString(3).toString(); // get fersons name 

                Object ob[]={imagedata,perName};
                dftm.addRow(ob);

                jTable1.getColumnModel().getColumn(0).setCellRenderer(new ImageRender());

                jTable1.setRowHeight(80);
 
            }
            conn.close();
 
        }
        
        catch (SQLException ex) {
              
            System.out.println(ex);
        }
        catch(Exception ex)
        {
            System.out.println(ex);
        }
       
       
}
    public void setTabledetailsByLname(String name,JTable jTable1) throws SQLException
{
 
        try 
            
        {
            DefaultTableModel dftm = (DefaultTableModel) jTable1.getModel();
 
            while(dftm.getRowCount()>0)
            {
                dftm.removeRow(0);
            }
             
            DBConnManager dbconn = new DBConnManager();
            Connection conn = dbconn.getConnect();
            
            Statement stmt = conn.createStatement();
            
            ResultSet rst = stmt.executeQuery("SELECT image,F_name,L_name FROM person WHERE L_name LIKE '"+name+"%'  ");
            
            while(rst.next())
            {
                
                byte[]imagedata = rst.getBytes(1); // get image data to byte array

                String perName = rst.getString(2).toString()+" "+rst.getString(3).toString(); // get fersons name 

                Object ob[]={imagedata,perName};
                dftm.addRow(ob);

                jTable1.getColumnModel().getColumn(0).setCellRenderer(new ImageRender());

                jTable1.setRowHeight(80);
 
            }
            conn.close();
 
        }
        
        catch (SQLException ex) {
              
            System.out.println(ex);
        }
        catch(Exception ex)
        {
            System.out.println(ex);
        }
       
       
}
    
    public void setTabledetailsByNumber(String name,JTable jTable1) throws SQLException
{
 
        try 
            
        {
            DefaultTableModel dftm = (DefaultTableModel) jTable1.getModel();
 
            while(dftm.getRowCount()>0)
            {
                dftm.removeRow(0);
            }
             
            DBConnManager dbconn = new DBConnManager();
            Connection conn = dbconn.getConnect();
            
            Statement stmt = conn.createStatement();
            
            ResultSet rst = stmt.executeQuery("SELECT image,F_name,L_name FROM person WHERE F_name LIKE '"+name+"%'  ");
            
            while(rst.next())
            {
                
                byte[]imagedata = rst.getBytes(1); 

                String perName = rst.getString(2).toString()+" "+rst.getString(3).toString(); 

                Object ob[]={imagedata,perName};
                dftm.addRow(ob);

                jTable1.getColumnModel().getColumn(0).setCellRenderer(new ImageRender());

                jTable1.setRowHeight(80);
 
            }
            conn.close();
 
        }
        
        catch (SQLException ex) {
              
            System.out.println(ex);
        }
        catch(Exception ex)
        {
            System.out.println(ex);
        }
       
       
}
    
    
     public static boolean  isEmpty(String str)
    {
        boolean isEmpty =false ;
        if(str == null || str.trim().isEmpty() )
        {
            isEmpty = true;
        }
        else
        {
            isEmpty = false;
        }
            
            
        return isEmpty;
    }
    
    
}
