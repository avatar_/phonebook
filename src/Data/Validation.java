/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

 
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Niroshan
 */
public class Validation {
    
  public static  boolean EmailValidation(String emailAddress)
    {
        String email = emailAddress.toString();
        boolean valide = false;
        
        if(isEmpty(email))
        {
            valide = true;
        }
         else
        {
        String email_pattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
		+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        
 
        Pattern pattern1 = Pattern.compile(email_pattern);
        Matcher match = pattern1.matcher(email);
        
            if(match.matches())
            {
                valide = true;
            }
            else
            {
                valide = false;
                
            }
        }
        
        return valide;
    }
  
  public static boolean TelephoneValidator( String val)
  {
      String telephone = val;
      boolean valid = false;
      if(isEmpty(val))
      {
          valid = true;
      }
      else
      {
      String TP_pattern = "[0-9]\\d{2}-\\d{7}";
      Pattern pattern1 = Pattern.compile(TP_pattern);
      Matcher match = pattern1.matcher(telephone);
          if(match.matches() == true)
          {
              valid = true;
          }
          else
          {
              valid = false;
              
          }
      
      }
      
      return valid;
  }
  
   
  
   public static boolean  isEmpty(String str)
    {
        boolean isEmpty =false ;
        if(str == null || str.trim().isEmpty() )
        {
            isEmpty = true;
        }
        else
        {
            isEmpty = false;
        }
            
            
        return isEmpty;
    }
   ///////////////////////////////////////////////////////////////////////////////////////////
   
  
}
